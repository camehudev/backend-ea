from passlib.hash import pbkdf2_sha256

def gerar_hash(texto_plano):
    return pbkdf2_sha256.hash(texto_plano)


def verifcar_hash(texto_plano, hash_plano):
    return pbkdf2_sha256.verify(texto_plano, hash_plano)