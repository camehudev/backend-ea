from typing import Annotated
from fastapi import Depends, FastAPI, HTTPException, Request, requests, Form, status
from fastapi.encoders import jsonable_encoder
from fastapi.security import OAuth2PasswordBearer
import dataBancos, classTabelas, json
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import classTabelas
import Providers.TokenProvider
import Providers.HashProvider

from hashlib import sha256


app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


origins = ['http://localhost:5173',
           'http://127.0.0.1:5173',
           'http://127.0.0.1:8000/api/livros/upload/',
           'http://127.0.0.1:8000/api/v1/user/auth/login', 
           'http://localhost:8000/api/v1/user/auth/login', 
           'http://localhost:8000//api/v1/user/cadastrar',
            'http://127.0.0.1:8000//api/v1/user/cadastrar',
            'http://127.0.0.1:8000/api/v1/token',
            'http://localhost:8000/api/v1/token',
                    
           ]

app.add_middleware(
    CORSMiddleware,
    allow_origins= origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class Upload(BaseModel):
    nomeArquivo: str
    bancoUsado: str
    
class DelItem(BaseModel):
    item_del : int
    bancoItem: str

class ConsultarItem(BaseModel):
    bancoItem: str
    
class NovoUsuario(BaseModel):
    firstName: str
    lastName: str
    nickName: str
    senhaUser: str
    userEmail: str
    
class UserLogin(BaseModel):
    nickName: str
    senhaUser: str
    
class TokenValidar(BaseModel):
    idToken:str
    
 
@app.get('/')
async def root():
   return 'PAGINA INICIAL'

# Estes Endpoints fazem parte da verificação do Usuario e Token
@app.post('/api/v1/user/auth/login')
async def login(user: UserLogin):
    try:             
        userOne =  classTabelas.Usuarios.ConsultarUser(None,f'{user.nickName}')
        if (Providers.HashProvider.verifcar_hash(user.senhaUser, userOne.userSenha)):
            tokengerar = Providers.TokenProvider.token(f'{userOne.nickName}',f'{userOne.lastName}',f'{userOne.userEmail}')
            return 'Usuário autenticado com sucesso!', tokengerar
        else:
            return 'Senha inválida!'
    
    except:
        return 'Usuário inválido!'
    
    
@app.post('/api/v1/token')
async def validarToken(tokenValidar: TokenValidar):
    try:
        tokenTrueFalse= Providers.TokenProvider.decoToken(tokem=f'{tokenValidar.idToken}')
        return tokenTrueFalse      
    
    except:
        return 'Token Invalido'
    
    
@app.post('/api/v1/user/cadastrar')
async def cadastrar(user: NovoUsuario):
    try:        
        senhaHash = Providers.HashProvider.gerar_hash(f'{user.senhaUser}')
        cadastrarUser = classTabelas.Usuarios.InserirUser(self=None,firstName=f'{user.firstName}',lastName=f'{user.lastName}', nickName=f'{user.nickName}', userSenha=f'{senhaHash}', userEmail=f'{user.userEmail}')
        return cadastrarUser      
        # return user.firstName, user.lastName, user.nickName,user.senhaUser,user.userEmail
        
    except:
        return 'error'
 
# Estes Endpoints recebe o arquivo .XLSX
@app.post('/api/v1/upload')
# Terá 2 argumentos nome do arquivo e banco
async def uploadExcell(arqUload: Upload):         
    try:        
        dados = dataBancos.paramUpload(arqUload.nomeArquivo)  
        
        if arqUload.bancoUsado == 'nubank':
            def nubank():
                for i in range(len(dados)):  
                    yield classTabelas.Nubank.InserirLista(None,dados[i]['data'], dados[i]['descricao'], dados[i]['valor'])
    
            return nubank()
        
        elif arqUload.bancoUsado == 'next':
            def next():
                for i in range(len(dados)):
                    yield classTabelas.Next.InserirLista(None,dados[i]['data'], dados[i]['descricao'], dados[i]['valor'])
                
            return next()
        
        elif arqUload.bancoUsado == 'caixa':
            def caixa():
                for i in range(len(dados)):
                    yield classTabelas.CaixaEconomica.InserirLista(None,dados[i]['data'], dados[i]['descricao'], dados[i]['valor'])

            return caixa()
                           
        
    except:
            return status.HTTP_404_NOT_FOUND   
    
        
@app.get('/api/consultar/{itemBanco}')
async def consultar(itemBanco: str): 
    try:
       
        if (itemBanco == 'next'):
            bancoConsultar = classTabelas.Next.ConsultarLista()
            return bancoConsultar
        else:
            if (itemBanco == 'nubank'):
                bancoConsultar = classTabelas.Nubank.ConsultarLista()
                return bancoConsultar
            
            else:
                if (itemBanco == 'caixa'):
                    bancoConsultar = classTabelas.CaixaEconomica.ConsultarLista()
                    return bancoConsultar
                else:
                    return status.HTTP_404_NOT_FOUND
    except:
        return status.HTTP_404_NOT_FOUND   
    
       
@app.post('/api/delete')
async def deletarItemLista(item: DelItem): 
    try:
        if (item.bancoItem == 'next'):
            item_deletado = classTabelas.Next.DeletarList(item.item_del)
            return item_deletado 
        else:
            if (item.bancoItem == 'nunbak'):
                item_Id_deletado = classTabelas.Nubank.DeletarList(item.item_del)
                return item_Id_deletado
            
            else:
                if (item.bancoItem == 'nunbak'):
                    item_Id_deletado = classTabelas.CaixaEconomica.DeletarList(item.item_del)
                    return item_Id_deletado
                else:
                    return status.HTTP_404_NOT_FOUND
    except:
        return status.HTTP_404_NOT_FOUND     
    
    
if __name__ == '__mail__':
    app.run()
    





    
  
    
  



