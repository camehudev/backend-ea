from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base,Session
from sqlalchemy import Column, Integer, String
# import psycopg2

# engine = create_engine('postgresql+psycopg2://postgres:CAme123/@localhost:5432/postgres')
engine = create_engine('sqlite:///ea.db', echo=True)

Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

class User(Base):
    __tablename__ = 'usuarios'

    idUsuario = Column( Integer, primary_key=True, autoincrement=True)
    firstName = Column(String(100))
    lastName = Column(String(100))
    nickName = Column(String(200))
    userSenha = Column(String(10000))
    userEmail = Column(String(200))   

    def __repr__(self):
        return f'User {self.idUsuario, self.lastName, self.nickName, self.userSenha, self.userEmail}'
    
    
class Nubank(Base):        
    __tablename__ = 'nubank'

    idBanco = Column( Integer, primary_key=True, autoincrement=True)
    dataLancamento= Column(String(100))
    historico = Column(String(50))
    valorLancamento = Column(String(100))
    
    def __repr__(self):
        return f'Nubank {self.idBanco,self.dataLancamento, self.historico, self.valorLancamento}'
    
    
class Next(Base):        
    __tablename__ = 'next'

    idBanco = Column( Integer, primary_key=True, autoincrement=True)
    dataLancamento= Column(String(100))
    historico = Column(String(50))
    valorLancamento = Column(String(100))
    
    def __repr__(self):
        return f'Next {self.idBanco,self.dataLancamento, self.historico, self.valorLancamento}'
    

class Caixa(Base):        
    __tablename__ = 'caixa'

    idBanco = Column( Integer, primary_key=True, autoincrement=True)
    dataLancamento= Column(String(100))
    historico = Column(String(50))
    valorLancamento = Column(String(100))
    
    def __repr__(self):
        return f'Caixa {self.idBanco,self.dataLancamento, self.historico, self.valorLancamento}'
   

Base.metadata.create_all(engine)